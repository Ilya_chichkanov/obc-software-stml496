/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdbool.h"
#include "log_utils.h"
#include "log_sd.h"
#include "logbook.h"
#include "repo_cmd.h"
#include "ring_buffer.h"
#include "unican.h"
#include "status.h"
#include "default_settings.h"
#include "iwdg.h"
#include "errors.h"
#include "telemetry.h"
#include "../../Application/Drivers/Imu_sens.h"
#include "../../Application/Drivers/GPS.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define HOUSEKEEPING_BUFFER_SIZE  50
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
/*

*/

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
SemaphoreHandle_t      I2C_mtx;
StaticSemaphore_t      I2C_mtx_buffer;
uint8_t                housekeeping_buffer_mem[HOUSEKEEPING_BUFFER_SIZE];
StaticMessageBuffer_t  housekeeping_buffer;
/* USER CODE END Variables */
osThreadId defaultTaskHandle;
uint32_t defaultTaskBuffer[ 128 ];
osStaticThreadDef_t defaultTaskControlBlock;
osThreadId dispatcherTaskHandle;
uint32_t dispatcherTaskBuffer[ 512 ];
osStaticThreadDef_t dispatcherTaskControlBlock;
osThreadId telemetryTaskHandle;
uint32_t telemetryTaskBuffer[ 768 ];
osStaticThreadDef_t telemetryTaskControlBlock;
osThreadId housekeepingTaskHandle;
uint32_t housekeepingTaskBuffer[ 128 ];
osStaticThreadDef_t housekeepingTaskControlBlock;
osThreadId hearbeatTaskHandle;
uint32_t hearbeatTaskBuffer[ 128 ];
osStaticThreadDef_t hearbeatTaskControlBlock;
osThreadId plannerTaskHandle;
uint32_t plannerTaskBuffer[ 256 ];
osStaticThreadDef_t plannerTaskControlBlock;
osThreadId ADCS_taskHandle;
uint32_t ADCS_taskBuffer[ 1024 ];
osStaticThreadDef_t ADCS_taskControlBlock;
osThreadId ISLTaskHandle;
uint32_t ISLTaskBuffer[ 256 ];
osStaticThreadDef_t ISLTaskControlBlock;
osThreadId UHF_telemertyHandle;
uint32_t UHF_telemertyBuffer[ 256 ];
osStaticThreadDef_t UHF_telemertyControlBlock;
osThreadId IWDGTaskHandle;
uint32_t IWDGTaskBuffer[ 128 ];
osStaticThreadDef_t IWDGTaskControlBlock;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);
void StartDispatcherTask(void const * argument);
void StartTelemetryTask(void const * argument);
void StartHousekeepingTask(void const * argument);
void StartHearbeatTask(void const * argument);
void StartPlannerTask(void const * argument);
void StartADCS_Task(void const * argument);
void ISLTaskStart(void const * argument);
void UHF_telemertyStart(void const * argument);
void StartTaskIWDG(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
  *ppxIdleTaskStackBuffer = &xIdleStack[0];
  *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
  /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
	I2C_mtx = xSemaphoreCreateMutexStatic(&I2C_mtx_buffer);
	xSemaphoreGive(I2C_mtx);

  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
	xMessageBufferCreateStatic(sizeof(housekeeping_buffer_mem), housekeeping_buffer_mem,
							   &housekeeping_buffer);
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadStaticDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128, defaultTaskBuffer, &defaultTaskControlBlock);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of dispatcherTask */
  osThreadStaticDef(dispatcherTask, StartDispatcherTask, osPriorityHigh, 0, 512, dispatcherTaskBuffer, &dispatcherTaskControlBlock);
  dispatcherTaskHandle = osThreadCreate(osThread(dispatcherTask), NULL);

  /* definition and creation of telemetryTask */
  osThreadStaticDef(telemetryTask, StartTelemetryTask, osPriorityNormal, 0, 768, telemetryTaskBuffer, &telemetryTaskControlBlock);
  telemetryTaskHandle = osThreadCreate(osThread(telemetryTask), NULL);

  /* definition and creation of housekeepingTask */
  osThreadStaticDef(housekeepingTask, StartHousekeepingTask, osPriorityNormal, 0, 128, housekeepingTaskBuffer, &housekeepingTaskControlBlock);
  housekeepingTaskHandle = osThreadCreate(osThread(housekeepingTask), NULL);

  /* definition and creation of hearbeatTask */
  osThreadStaticDef(hearbeatTask, StartHearbeatTask, osPriorityNormal, 0, 128, hearbeatTaskBuffer, &hearbeatTaskControlBlock);
  hearbeatTaskHandle = osThreadCreate(osThread(hearbeatTask), NULL);

  /* definition and creation of plannerTask */
  osThreadStaticDef(plannerTask, StartPlannerTask, osPriorityNormal, 0, 256, plannerTaskBuffer, &plannerTaskControlBlock);
  plannerTaskHandle = osThreadCreate(osThread(plannerTask), NULL);

  /* definition and creation of ADCS_task */
  osThreadStaticDef(ADCS_task, StartADCS_Task, osPriorityNormal, 0, 1024, ADCS_taskBuffer, &ADCS_taskControlBlock);
  ADCS_taskHandle = osThreadCreate(osThread(ADCS_task), NULL);

  /* definition and creation of ISLTask */
  osThreadStaticDef(ISLTask, ISLTaskStart, osPriorityNormal, 0, 256, ISLTaskBuffer, &ISLTaskControlBlock);
  ISLTaskHandle = osThreadCreate(osThread(ISLTask), NULL);

  /* definition and creation of UHF_telemerty */
  osThreadStaticDef(UHF_telemerty, UHF_telemertyStart, osPriorityNormal, 0, 256, UHF_telemertyBuffer, &UHF_telemertyControlBlock);
  UHF_telemertyHandle = osThreadCreate(osThread(UHF_telemerty), NULL);

  /* definition and creation of IWDGTask */
  osThreadStaticDef(IWDGTask, StartTaskIWDG, osPriorityHigh, 0, 128, IWDGTaskBuffer, &IWDGTaskControlBlock);
  IWDGTaskHandle = osThreadCreate(osThread(IWDGTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  //osTimerStart(heartbeatHandle, 3000);
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
	const char * tag = "DefaultTask";
	const TickType_t xFrequency = 1000;
	TickType_t xLastWakeTime = xTaskGetTickCount();
	LOGI(tag,"Start");
	for(;;)
	{
		DEBUG_TOGGLE
		vTaskDelayUntil( &xLastWakeTime, xFrequency );
	}


  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_StartDispatcherTask */
struct unican_handler command_dispatcher;
/**
* @brief Function implementing the dispatcherTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartDispatcherTask */
void StartDispatcherTask(void const * argument)
{
  /* USER CODE BEGIN StartDispatcherTask */
  /* Infinite loop */
	static const char *tag = "OBC dispatcher task";
	LOGI(tag,"Start");
	unican_add_handler(UNICAN_HANDLERID_GROUND, &command_dispatcher);
	struct cmd_t cmd_new;
	uint8_t cmd_table_id;
	uint16_t cmd_res;
	struct status* dev_status = status_get();
	for(;;)
	{
		if (unican_receive(&command_dispatcher, portMAX_DELAY)) {
			uint16_t res_check = cmd_check(command_dispatcher.packet.msg_id,command_dispatcher.packet.data_len,&cmd_table_id);
			if(res_check==CMD_OK){
				cmd_new.id_cmd = cmd_table_id;
				cmd_new.sender = command_dispatcher.packet.sender;
				cmd_new.args   = (command_dispatcher.packet.data_len>0)?command_dispatcher.packet.data:NULL;
				cmd_res = cmd_run(cmd_new.id_cmd,cmd_new.sender, cmd_new.args);
				if(cmd_res==0){
					unican_ack(cmd_new.sender,command_dispatcher.packet.msg_id);
				}
				else{
					unican_nack(cmd_new.sender,command_dispatcher.packet.msg_id,cmd_res);
				}
			}
			else if(res_check==CMD_NOT_FOUND){
				unican_nack(command_dispatcher.packet.sender,command_dispatcher.packet.msg_id,ERR_ANKNOWN_CMD);
				LOGI(tag,"UNKNOWN COMMAND");
			}
			else{
				unican_nack(command_dispatcher.packet.sender,command_dispatcher.packet.msg_id,ERR_CMD_WRONG_PARAM);
				LOGI(tag,"WRONG PARAM");
			}
			if (dev_status->sens_err[SENS_OBC_SD_CARD]==0){
			  char result[50] = "";
			  sprintf(result, "Cmd %d check %d, res %d \n",command_dispatcher.packet.msg_id,res_check,cmd_res);
			  uint16_t error_id = log_sd_str("Events.txt", &result, sizeof(result));
			  dev_status->sens_err[SENS_OBC_SD_CARD] = error_id==0? 0: ERR_SD_CARD_WRITE;
			}
		}
	}


  /* USER CODE END StartDispatcherTask */
}

/* USER CODE BEGIN Header_StartTelemetryTask */
/**
* @brief Function implementing the telemetryTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTelemetryTask */
void StartTelemetryTask(void const * argument)
{
  /* USER CODE BEGIN StartTelemetryTask */
  /* Infinite loop */
	static const char *tag = "OBC telemerty";
	TickType_t xFrequency = DEFAULT_TEL_REG_PERIOD;
	TickType_t xLastWakeTime;
	LOGI(tag,"START");
	xLastWakeTime = xTaskGetTickCount();
	struct status* dev_status = status_get();
	uint16_t error_id;
	for(;;)
	{
		if (xSemaphoreTake(I2C_mtx, 100)== pdTRUE){
		  telemerty_regylar();
		  struct telemerty_reg *tel_reg = telemerty_regular_get();
		  if (dev_status->sens_err[SENS_OBC_SD_CARD]==0){
			  error_id = log_sd_int_array("Tempetarures.txt",tel_reg->temperature_param,T_SENS_OBC_LAST-T_SENS_OBC_FIRST+1)||
					     log_sd_float_array("Imu.txt",tel_reg->imu_param,IMU_PARAM_LAST - IMU_PARAM_FIRST+1)||
						 log_sd_int_array("Current.txt",tel_reg->current_param,UI_SENSORS_OBC_LAST - UI_SENSORS_OBC_FIRST+1)||
						 log_sd_int_array("Voltage.txt",tel_reg->voltage_param,UI_SENSORS_OBC_LAST - UI_SENSORS_OBC_FIRST+1);
			  dev_status->sens_err[SENS_OBC_SD_CARD] = error_id==0? 0: ERR_SD_CARD_WRITE;
			  LOGI(tag,"LOG TELEMETRY");
		  }
		  xSemaphoreGive(I2C_mtx);
		  vTaskDelayUntil( &xLastWakeTime, xFrequency );
		}

	}


  /* USER CODE END StartTelemetryTask */
}

/* USER CODE BEGIN Header_StartHousekeepingTask */
/**
* @brief Function implementing the housekeepingTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartHousekeepingTask */
void StartHousekeepingTask(void const * argument)
{
  /* USER CODE BEGIN StartHousekeepingTask */
  /* Infinite loop */
	static const char *tag = "Housekeeping";
	LOGI(tag,"START");
	uint16_t error_id;
	for(;;)
	{
		size_t n_bytes = xMessageBufferReceive(&housekeeping_buffer, &error_id, sizeof(uint8_t), pdMS_TO_TICKS(portMAX_DELAY));
		if(n_bytes){
			struct Logbook_t logbook;
			logbook.error = error_id;
			logbook_write(&logbook);
		}
	}
  /* USER CODE END StartHousekeepingTask */
}

/* USER CODE BEGIN Header_StartHearbeatTask */
/**
* @brief Function implementing the hearbeatTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartHearbeatTask */
void StartHearbeatTask(void const * argument)
{
  /* USER CODE BEGIN StartHearbeatTask */
  /* Infinite loop */
	static const char *tag = "Hearbeat";
	TickType_t xFrequency = DEFOULT_HEATBEAT_PEROOD;
	TickType_t xLastWakeTime;
	LOGI(tag,"START");
	xLastWakeTime = xTaskGetTickCount();
	for(;;)
	{
	  unican_hearbeat(0x18);
	  vTaskDelayUntil( &xLastWakeTime, xFrequency );
	}
  /* USER CODE END StartHearbeatTask */
}

/* USER CODE BEGIN Header_StartPlannerTask */
/**
* @brief Function implementing the plannerTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartPlannerTask */
void StartPlannerTask(void const * argument)
{
  /* USER CODE BEGIN StartPlannerTask */
  /* Infinite loop */
	static const char *tag = "Planner Task";
	TickType_t xFrequency = DEFAULT_PLANNER_PERIOD;
	TickType_t xLastWakeTime;
	LOGI(tag,"START");
	xLastWakeTime = xTaskGetTickCount();
	for(;;)
	{
		vTaskDelayUntil( &xLastWakeTime, xFrequency );
	}
  /* USER CODE END StartPlannerTask */
}

/* USER CODE BEGIN Header_StartADCS_Task */
/**
* @brief Function implementing the ADCS_task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartADCS_Task */
void StartADCS_Task(void const * argument)
{
  /* USER CODE BEGIN StartADCS_Task */
  /* Infinite loop */
	static const char *tag = "ADCS Task";
	TickType_t xFrequency = 10000;
	TickType_t xLastWakeTime;
	LOGI(tag,"START");
	xLastWakeTime = xTaskGetTickCount();
	struct gps_data data_gps;
	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate;
	for(;;)
	{
		if (xSemaphoreTake(I2C_mtx, 100)== pdTRUE){
			 bool res = GPS_get_data(&data_gps, 2000);
			 if(res){
				 LOGI("tag", "GPS OK");
				 sDate.Date  = data_gps.time.days;
				 sTime.Hours = data_gps.time.hours;
				 sTime.Minutes = data_gps.time.minutes;
				 sTime.Seconds = data_gps.time.seconds;
				 time_set(&sTime, &sDate);
			 }


			xSemaphoreGive(I2C_mtx);
		}
		vTaskDelayUntil( &xLastWakeTime, xFrequency );
	}

  /* USER CODE END StartADCS_Task */
}

/* USER CODE BEGIN Header_ISLTaskStart */
//struct unican_handler ISL_dispatcher;
/**
* @brief Function implementing the ISLTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_ISLTaskStart */
void ISLTaskStart(void const * argument)
{
  /* USER CODE BEGIN ISLTaskStart */
  /* Infinite loop */
	static const char *tag = "ISL Task";
	TickType_t xFrequency = 4000;
	TickType_t xLastWakeTime;
	LOGI(tag,"START");
	xLastWakeTime = xTaskGetTickCount();
	struct telemerty_reg *tel_reg;
	for(;;)
	{
/*		if (unican_receive(&ISL_dispatcher, portMAX_DELAY)) {

		}*/
		vTaskDelayUntil( &xLastWakeTime, xFrequency );
	}

  /* USER CODE END ISLTaskStart */
}

/* USER CODE BEGIN Header_UHF_telemertyStart */
/**
* @brief Function implementing the UHF_telemerty thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_UHF_telemertyStart */
void UHF_telemertyStart(void const * argument)
{
  /* USER CODE BEGIN UHF_telemertyStart */
  /* Infinite loop */
	static const char *tag = "UHF Telemerty";
	TickType_t xFrequency = 4000;
	TickType_t xLastWakeTime;
	LOGI(tag,"START");
	xLastWakeTime = xTaskGetTickCount();
	struct telemerty_reg *tel_reg;
	struct status * dev_status = status_get();
	for(;;)
	{
		if (xSemaphoreTake(I2C_mtx, 100)== pdTRUE){
			tel_reg = telemerty_regular_get();
			struct unican_message msg;
			msg.data = (uint8_t *)tel_reg;
			size_t s = sizeof(struct telemerty_reg);
			msg.unican_length = 28;
			msg.unican_msg_id = 17029;
			switch (dev_status->state)
			{
			case UHF:
				msg.unican_address_to = 0x1;
				LOGI(tag, "UHF");
				dev_status->state = ISL;
				break;
			case ISL:
				msg.unican_address_to = 0x1;
				LOGI(tag, "ISL");
				dev_status->state = UHF;
				break;
			default:
			   break;
			}
			unican_send_msg(&msg);
			xSemaphoreGive(I2C_mtx);
		}
		vTaskDelayUntil( &xLastWakeTime, xFrequency );
	}

  /* USER CODE END UHF_telemertyStart */
}

/* USER CODE BEGIN Header_StartTaskIWDG */
/**
* @brief Function implementing the IWDGTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartTaskIWDG */
void StartTaskIWDG(void const * argument)
{
  /* USER CODE BEGIN StartTaskIWDG */
  /* Infinite loop */
	static const char *tag = "IWDG";
	TickType_t xFrequency = DEFAULT_IWDG_PERIOD;
	TickType_t xLastWakeTime;
	LOGI(tag,"START");
	xLastWakeTime = xTaskGetTickCount();
	for(;;)
	{
		HAL_IWDG_Refresh(&hiwdg);
		vTaskDelayUntil( &xLastWakeTime, xFrequency );
	}
  /* USER CODE END StartTaskIWDG */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
