#include "GPS.h"
#include "log_utils.h"
#include "stream_buffer.h"
#include "stdio.h"
#include "string.h"

static char * tag = "gps";

static struct {
	UART_HandleTypeDef * uart;
	uint8_t				 dma_storage[N_GPS_DATA_DMA];
    uint8_t              BufferStorage[N_GPS_STREAM_BUF_SIZE];
    StaticStreamBuffer_t StreamBuffer_struct;
    StreamBufferHandle_t StreamBuffer;
} GPS_state;

static void uart_rx_cb(void);


void GPS_module_init(UART_HandleTypeDef * huart){
	GPS_state.uart = huart;
	HAL_UART_RegisterCallback(GPS_state.uart, HAL_UART_RX_COMPLETE_CB_ID, uart_rx_cb);
	GPS_state.StreamBuffer = xStreamBufferCreateStatic( sizeof( GPS_state.BufferStorage ),
	                                               1,
												   GPS_state.BufferStorage,
	                                               &GPS_state.StreamBuffer_struct );

}

bool GPS_get_data(struct gps_data *data, uint32_t timeout){
	HAL_UART_Receive_DMA(GPS_state.uart, &GPS_state.dma_storage, N_GPS_DATA_DMA-1);
	size_t xReceivedBytes = 0;
	uint8_t raw_data[N_GPS_STREAM_BUF_SIZE];
	xReceivedBytes = xStreamBufferReceive(GPS_state.StreamBuffer,
											( void * ) raw_data,
											 N_GPS_STREAM_BUF_SIZE,
											 pdMS_TO_TICKS(timeout) );
	uint8_t j = 0;
	uint8_t  gps_flags=0;
	uint32_t year=0, month=0, days = 0,hours = 0,minutes = 0,seconds = 0;
	uint64_t latitude=0, longitude=0, altitude=0;

	for (j =0; j <8; j++)
	{
		latitude = latitude + ((uint64_t)(raw_data[GPS_LATITUDE_POS*2+j]) << 8*j);
		longitude = longitude + ((uint64_t)raw_data[GPS_LONGITUDE_POS*2+j] << 8*j);
		altitude = altitude + ((uint64_t)raw_data[GPS_ALTITUDE_POS*2+j] << 8*j);
	}
	for (j =0; j <2; j++)
	{
		year  = year  + (raw_data[GPS_YAER_POS*2+j] << 8*j);
		month = month + (raw_data[GPS_MONTH_POS*2+j] << 8*j);
		days  = days  + (raw_data[GPS_DAY_POS*2+j] << 8*j);
		hours = hours + (raw_data[GPS_HOUR_POS*2+j] << 8*j);
		minutes = minutes  + (raw_data[GPS_MINUTE_POS*2+j] << 8*j);
		seconds = seconds  + (raw_data[GPS_SECOND_POS*2+j] << 8*j);
	}
	gps_flags = raw_data[GPS_FLAGS_POS*2];
	bool valid_time = gps_flags&(1<<4);
	bool valid_solution = gps_flags&(1<<5);
	memcpy(&(data->latitude), &latitude, 8);
	memcpy(&(data->longitude), &longitude,8 );
	memcpy(&(data->altitude), &altitude, 8 );
	data->time.days = days;
	data->time.hours = hours;
	data->time.minutes = minutes;
	data->time.seconds = seconds;
	return valid_time&&valid_solution;
}


void uart_rx_cb(void){

	uint16_t i = 0;
	while (i <= N_GPS_DATA)
	{
		uint16_t gps_frame_id;
		if ((GPS_state.dma_storage[i] == GPS_SYNC_WORD_R) && (GPS_state.dma_storage[i+1] == GPS_SYNC_WORD_L))
		{
			gps_frame_id = GPS_state.dma_storage[i + GPS_FRAME_ID_NUMBER*2] + (GPS_state.dma_storage[i + GPS_FRAME_ID_NUMBER*2 + 1] << 8);
			//dma_storage_size = GPS_state.dma_storage[i + GPS_DATA_SIZE_NUMBER*2] + (GPS_state.dma_storage[i + GPS_DATA_SIZE_NUMBER*2 + 1] << 8);
			if (gps_frame_id == GPS_FRAME_ID_UTC)
			{
				BaseType_t xHigherPriorityTaskWoken = pdFALSE;
				size_t xBytesSent = xStreamBufferSendFromISR( GPS_state.StreamBuffer,
				                                             ( void *)&GPS_state.dma_storage[i + GPS_FRAME_HEADER_BYTES],
															 N_GPS_STREAM_BUF_SIZE,
				                                             &xHigherPriorityTaskWoken);
				break;
			}
		}
		i++;
	}
}
