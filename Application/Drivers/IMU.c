/*
 * imu.c
 *
 *  Created on: 7
 *      Author: hz
 */

#include "IMU.h"
#include "usart.h"

#define MAGN_ADDR 0x1e
#define GYRO_ADDR 0x6b
#define SAT_NUMBER 2

extern UART_HandleTypeDef *muart;
I2C_HandleTypeDef *i2c_LSM9DS1;

int16_t a[3][3];
int16_t b[3];
float gyro_offset[3];

float a1 = 1;
float a2 = 2;
float a3 = 3;

void IMU_i2cread(uint8_t device) {
    //uint16_t addr = device<<8 |shift;
    uint16_t addr = device;
    uint8_t ans;
    //uint8_t aTxBuffer = 0x0;
    if (HAL_I2C_Master_Receive(i2c_LSM9DS1, addr, (uint8_t *) &ans, (uint16_t) 1, (uint32_t) 1000) !=
        HAL_OK); //R 0xD1 0x6B noack
    //error_id = ERR_IMU_READING;//HAL_UART_Transmit(muart,(uint8_t*)"READING noack\n\r",15, 0xFF);
}

void IMU_i2cwrite(uint8_t device, uint8_t shift, uint8_t data, uint8_t size) {
    uint16_t addr = device;//<<8 |shift;

    uint8_t aTxBuffer[2] = {shift, data};
    if (HAL_I2C_Master_Transmit(i2c_LSM9DS1, addr, (uint8_t *) &aTxBuffer, (uint16_t) size + 1, (uint32_t) 1000) !=
        HAL_OK); //W 0xD0 0x6B
    //error_id = ERR_IMU_WRITING;//HAL_UART_Transmit(muart,(uint8_t*)"WRITING ERROR\n\r",15, 0xFF);
}

int IMU_Init(I2C_HandleTypeDef *hi2c) {
    //check whoAmI
    i2c_LSM9DS1 = hi2c;
    uint8_t buffer[2];

    buffer[0] = 0x0f;
    if (HAL_I2C_Master_Transmit(i2c_LSM9DS1, MAGN_ADDR << 1, (uint8_t *) buffer, 1, (uint32_t) 1000) != HAL_OK){
    //error_id = ERR_IMU_WRITING;//HAL_UART_Transmit(muart,(uint8_t*)"WRITING ERROR\n\r",15, 0xFF);
    	return 6;
    }
    if (HAL_I2C_Master_Receive(i2c_LSM9DS1, MAGN_ADDR << 1, (uint8_t *) buffer, 1, (uint32_t) 1000) != HAL_OK);
    //error_id = ERR_IMU_READING;//HAL_UART_Transmit(muart,(uint8_t*)"READING NOACK\n\r",15, 0xFF);
    if (buffer[0] == 0b00111101)
        HAL_UART_Transmit(muart, (uint8_t *) "MAGNETOMETER OK\n\r", 17, 0xFF);

    //else
    //error_id = ERR_IMU_MAG;//

    buffer[0] = 0x0f;
    if (HAL_I2C_Master_Transmit(i2c_LSM9DS1, GYRO_ADDR << 1, (uint8_t *) buffer, 1, (uint32_t) 1000) != HAL_OK);
    //error_id = ERR_IMU_WRITING;//HAL_UART_Transmit(muart,(uint8_t*)"WRITING ERROR\n\r",15, 0xFF);

    if (HAL_I2C_Master_Receive(i2c_LSM9DS1, GYRO_ADDR << 1, (uint8_t *) buffer, 1, (uint32_t) 1000) != HAL_OK);
    //error_id = ERR_IMU_READING;//HAL_UART_Transmit(muart,(uint8_t*)"READING NOACK\n\r",15, 0xFF);
    if (buffer[0] == 0b01101000)
        HAL_UART_Transmit(muart, (uint8_t *) "ACCEL+GYRO OK\n\r", 15, 0xFF);
    //else
    //	error_id = ERR_IMU_GYRO;


    //settings reset
    buffer[0] = 0x21;
    buffer[1] = 0b00000100;
    if (HAL_I2C_Master_Transmit(i2c_LSM9DS1, MAGN_ADDR << 1, (uint8_t *) buffer, 2, (uint32_t) 1000) != HAL_OK)
        //error_id = ERR_IMU_WRITING;
        HAL_UART_Transmit(muart, (uint8_t *) "WRITING ERROR\n\r", 15, 0xFF);

//	buffer[0] = 0x22;
//	buffer[1] = 0b00000001;
//	if (HAL_I2C_Master_Transmit(i2c_LSM9DS1, GYRO_ADDR<<1, (uint8_t*) buffer, 2, (uint32_t)1000)!= HAL_OK)
    //		HAL_UART_Transmit(muart,(uint8_t*)"WRITING ERROR\n\r",15, 0xFF);

    //settings
    buffer[0] = 0x20;
    buffer[1] = 0b10010000;
    if (HAL_I2C_Master_Transmit(i2c_LSM9DS1, GYRO_ADDR << 1, (uint8_t *) buffer, 2, (uint32_t) 1000) != HAL_OK)
        //error_id = ERR_IMU_WRITING;
        HAL_UART_Transmit(muart, (uint8_t *) "WRITING ERROR\n\r", 15, 0xFF);

    buffer[0] = 0x10;
    buffer[1] = 0b10001000;
    if (HAL_I2C_Master_Transmit(i2c_LSM9DS1, GYRO_ADDR << 1, (uint8_t *) buffer, 2, (uint32_t) 1000) != HAL_OK)
        //error_id = ERR_IMU_WRITING;
        HAL_UART_Transmit(muart, (uint8_t *) "WRITING ERROR\n\r", 15, 0xFF);

    buffer[0] = 0x21;
    buffer[1] = 0b00000000;
    if (HAL_I2C_Master_Transmit(i2c_LSM9DS1, MAGN_ADDR << 1, (uint8_t *) buffer, 2, (uint32_t) 1000) != HAL_OK)
        //error_id = ERR_IMU_WRITING;
        HAL_UART_Transmit(muart, (uint8_t *) "WRITING ERROR\n\r", 15, 0xFF);

//imu: 10; 20; 12

//magn 20; 22, 23

    buffer[0] = 0x20;
    buffer[1] = 0b11011000;
    if (HAL_I2C_Master_Transmit(i2c_LSM9DS1, MAGN_ADDR << 1, (uint8_t *) buffer, 2, (uint32_t) 1000) != HAL_OK)
        //error_id = ERR_IMU_WRITING;
        HAL_UART_Transmit(muart, (uint8_t *) "WRITING ERROR\n\r", 15, 0xFF);

    buffer[0] = 0x21;
    buffer[1] = 0b00000000;
    if (HAL_I2C_Master_Transmit(i2c_LSM9DS1, MAGN_ADDR << 1, (uint8_t *) buffer, 2, (uint32_t) 1000) != HAL_OK)
        //error_id = ERR_IMU_WRITING;
        HAL_UART_Transmit(muart, (uint8_t *) "WRITING ERROR\n\r", 15, 0xFF);

    buffer[0] = 0x22;
    buffer[1] = 0b00000000;
    if (HAL_I2C_Master_Transmit(i2c_LSM9DS1, MAGN_ADDR << 1, (uint8_t *) buffer, 2, (uint32_t) 1000) != HAL_OK)
        //error_id = ERR_IMU_WRITING;
        HAL_UART_Transmit(muart, (uint8_t *) "WRITING ERROR\n\r", 15, 0xFF);

    buffer[0] = 0x23;
    buffer[1] = 0b00001000;
    if (HAL_I2C_Master_Transmit(i2c_LSM9DS1, MAGN_ADDR << 1, (uint8_t *) buffer, 2, (uint32_t) 1000) != HAL_OK)
        //error_id = ERR_IMU_WRITING;
        HAL_UART_Transmit(muart, (uint8_t *) "WRITING ERROR\n\r", 15, 0xFF);

    switch (SAT_NUMBER) {
        case 1: {
        }
        case 2: {
//		            b[0]= 0;
//		            b[1]= 0;
//		            b[2]= 0;

//		            a[0][0]= 1;
//		            a[0][1]= 0;
//		            a[0][2]= 0;
//		            a[1][0]= a[0][1];
//		            a[1][1]= 1;
//		            a[1][2]= 0;
//		            a[2][0]= a[0][2];
//		            a[2][1]= a[1][2];
//		            a[2][2]= 1;

            b[0] = -371;
            b[1] = 1245;
            b[2] = -1828;
//
            a[0][0] = 1083;
            a[0][1] = 31;
            a[0][2] = -2;
            a[1][0] = a[0][1];
            a[1][1] = 1080;
            a[1][2] = 1;
            a[2][0] = a[0][2];
            a[2][1] = a[1][2];
            a[2][2] = 1066;

            gyro_offset[0] = 0;
            gyro_offset[1] = 0;
            gyro_offset[2] = 170;


        }
        case 3: {
        }
    }
    return 0;
}

void IMU_GetValue(int16_t *mag_x, int16_t *mag_y, int16_t *mag_z, int16_t *gyro_x, int16_t *gyro_y, int16_t *gyro_z,
                  int16_t *accel_x, int16_t *accel_y, int16_t *accel_z, int16_t *unc_mx, int16_t *unc_my,
                  int16_t *unc_mz) {
    uint8_t buffRx[8] = {0, 0, 0, 0, 0, 0, 0, 0};
    uint8_t buffTx[2] = {0, 0};
    int16_t dataMagnet[3];
    float dataGyro[3];
    //GYRO: 18-19, 1a-1b, 1c-1d
    //ACCEL: 28-29; 2a-2b; 2c-2d

    //MAGN: 28-29; 2a-2b; 2c-2d
    buffTx[0] = 0x28;
    if (HAL_I2C_Master_Transmit(i2c_LSM9DS1, MAGN_ADDR << 1, (uint8_t *) buffTx, 1, (uint32_t) 1000) != HAL_OK)
        //error_id = ERR_IMU_WRITING;
        HAL_UART_Transmit(muart, (uint8_t *) "WRITING ERROR\n\r", 15, 0xFF);
    if (HAL_I2C_Master_Receive(i2c_LSM9DS1, MAGN_ADDR << 1, (uint8_t *) buffRx, 6, (uint32_t) 1000) != HAL_OK)
        //error_id = ERR_IMU_READING;
        HAL_UART_Transmit(muart, (uint8_t *) "READING NOACK\n\r", 15, 0xFF);
    //get raw data
    dataMagnet[0] = buffRx[1] << 8 | buffRx[0];
    dataMagnet[1] = buffRx[3] << 8 | buffRx[2];
    dataMagnet[2] = buffRx[5] << 8 | buffRx[4];

    //Calibrate and return data
    *mag_y = (a[0][0] * (dataMagnet[0] - b[0]) + a[0][1] * (dataMagnet[1] - b[1]) + a[0][2] * (dataMagnet[2] - b[2])) /
             1000;
    *mag_x = -(a[1][0] * (dataMagnet[0] - b[0]) + a[1][1] * (dataMagnet[1] - b[1]) + a[1][2] * (dataMagnet[2] - b[2])) /
             1000;
    *mag_z = (a[2][0] * (dataMagnet[0] - b[0]) + a[2][1] * (dataMagnet[1] - b[1]) + a[2][2] * (dataMagnet[2] - b[2])) /
             1000;

    *unc_mx = (float) dataMagnet[0];
    *unc_my = (float) dataMagnet[1];
    *unc_mz = (float) dataMagnet[2];

    buffTx[0] = 0x18;
    if (HAL_I2C_Master_Transmit(i2c_LSM9DS1, GYRO_ADDR << 1, (uint8_t *) buffTx, 1, (uint32_t) 1000) != HAL_OK)
        //error_id = ERR_IMU_WRITING;
        HAL_UART_Transmit(muart, (uint8_t *) "WRITING ERROR\n\r", 15, 0xFF);
    if (HAL_I2C_Master_Receive(i2c_LSM9DS1, GYRO_ADDR << 1, (uint8_t *) buffRx, 6, (uint32_t) 1000) != HAL_OK)
        //error_id = ERR_IMU_READING;
        HAL_UART_Transmit(muart, (uint8_t *) "READING NOACK\n\r", 15, 0xFF);
    //get raw data
    dataGyro[0] = buffRx[1] << 8 | buffRx[0];
    dataGyro[1] = buffRx[3] << 8 | buffRx[2];
    //dataGyro[1] = buffRx[0];
    //dataGyro[2] = buffRx[1];
    dataGyro[2] = buffRx[5] << 8 | buffRx[4];

    //callibrate and return data
    *gyro_x = dataGyro[0] - gyro_offset[0];
    *gyro_y = dataGyro[1] - gyro_offset[1];
    *gyro_z = dataGyro[2] - gyro_offset[2];

    buffTx[0] = 0x28;
    if (HAL_I2C_Master_Transmit(i2c_LSM9DS1, GYRO_ADDR << 1, (uint8_t *) buffTx, 1, (uint32_t) 1000) != HAL_OK)
        //error_id = ERR_IMU_WRITING;
        HAL_UART_Transmit(muart, (uint8_t *) "WRITING ERROR\n\r", 15, 0xFF);
    if (HAL_I2C_Master_Receive(i2c_LSM9DS1, GYRO_ADDR << 1, (uint8_t *) buffRx, 6, (uint32_t) 1000) != HAL_OK)
        //error_id = ERR_IMU_READING;
        HAL_UART_Transmit(muart, (uint8_t *) "READING NOACK\n\r", 15, 0xFF);

    *accel_x = buffRx[1] << 8 | buffRx[0];
    *accel_y = buffRx[3] << 8 | buffRx[2];
    *accel_z = buffRx[5] << 8 | buffRx[4];

}
