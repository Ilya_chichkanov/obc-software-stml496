/*
 * Imu_sens.h
 *
 *  Created on: 9 апр. 2021 г.
 *      Author: Ilia
 */

#ifndef DRIVERS_IMU_SENS_H_
#define DRIVERS_IMU_SENS_H_
#include "stm32l4xx_hal.h"
uint8_t Imu_init(void);
uint8_t Imu_read_mag( float *mag_x, float *mag_y, float *mag_z);
uint8_t Imu_read_gyro_accel( float *gyro_x, float *gyro_y, float *gyro_z,
		float *accel_x, float *accel_y, float *accel_z);

#endif /* DRIVERS_IMU_SENS_H_ */
