/*
 * Imu_sens.c
 *
 *  Created on: 9 апр. 2021 г.
 *      Author: Ilia
 */


#include "Imu_sens.h"
#include "lsm9ds1_reg.h"
#include "string.h"
extern TIM_HandleTypeDef htim3;
extern I2C_HandleTypeDef hi2c1;

lsm9ds1_status_t reg;
stmdev_ctx_t dev_ctx_mag;
stmdev_ctx_t dev_ctx_imu;
uint8_t i2c_add_imu = LSM9DS1_IMU_I2C_ADD_H;
uint8_t i2c_add_mag = LSM9DS1_MAG_I2C_ADD_L;

int32_t I2c_write(void *handle, uint8_t reg, uint8_t *bufp,
                              uint16_t len)
{
  uint8_t *i2c_address = handle;
  HAL_I2C_Mem_Write(&hi2c1, *i2c_address, reg,
                    I2C_MEMADD_SIZE_8BIT, bufp, len, 1000);
  return 0;
}

int32_t I2c_read(void *handle, uint8_t reg, uint8_t *bufp,
                             uint16_t len)
{
  uint8_t *i2c_address = handle;
  HAL_I2C_Mem_Read(&hi2c1, *i2c_address, reg,
                   I2C_MEMADD_SIZE_8BIT, bufp, len, 1000);
  return 0;
}

uint8_t Imu_init(void)
{

  /* Initialize inertial sensors (IMU) driver interface */

  //stmdev_ctx_t dev_ctx_mag;
  dev_ctx_mag.write_reg = I2c_write;
  dev_ctx_mag.read_reg = I2c_read;
  dev_ctx_mag.handle = (void*)&i2c_add_mag;

  /* Initialize magnetic sensors driver interface */
  //stmdev_ctx_t dev_ctx_imu;
  dev_ctx_imu.write_reg = I2c_write;
  dev_ctx_imu.read_reg = I2c_read;
  dev_ctx_imu.handle = (void*)&i2c_add_imu;

  /* Check device ID */
  lsm9ds1_id_t whoamI;
  lsm9ds1_dev_id_get(&dev_ctx_mag, &dev_ctx_imu, &whoamI);
  if (whoamI.imu != LSM9DS1_IMU_ID || whoamI.mag != LSM9DS1_MAG_ID){
	 //HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_SET);
	 return 1; // return in case of error
  }

  /* Restore default configuration */
  lsm9ds1_dev_reset_set(&dev_ctx_mag, &dev_ctx_imu, PROPERTY_ENABLE);
  uint8_t rst;
  do {
    lsm9ds1_dev_reset_get(&dev_ctx_mag, &dev_ctx_imu, &rst);
  } while (rst);

  /* Enable Block Data Update */
  lsm9ds1_block_data_update_set(&dev_ctx_mag, &dev_ctx_imu, PROPERTY_ENABLE);


  /* Set full scale */
  lsm9ds1_xl_full_scale_set(&dev_ctx_imu, LSM9DS1_4g);
  lsm9ds1_gy_full_scale_set(&dev_ctx_imu, LSM9DS1_2000dps);
  lsm9ds1_mag_full_scale_set(&dev_ctx_mag, LSM9DS1_16Ga);

  /* Configure filtering chain - See datasheet for filtering chain details */
  /* Accelerometer filtering chain */
  lsm9ds1_xl_filter_aalias_bandwidth_set(&dev_ctx_imu, LSM9DS1_AUTO);
  lsm9ds1_xl_filter_lp_bandwidth_set(&dev_ctx_imu, LSM9DS1_LP_ODR_DIV_50);
  lsm9ds1_xl_filter_out_path_set(&dev_ctx_imu, LSM9DS1_LP_OUT);
  /* Gyroscope filtering chain */
  lsm9ds1_gy_filter_lp_bandwidth_set(&dev_ctx_imu, LSM9DS1_LP_ULTRA_LIGHT);
  lsm9ds1_gy_filter_hp_bandwidth_set(&dev_ctx_imu, LSM9DS1_HP_MEDIUM);
  lsm9ds1_gy_filter_out_path_set(&dev_ctx_imu, LSM9DS1_LPF1_HPF_LPF2_OUT);

  /* Set Output Data Rate / Power mode */
  lsm9ds1_imu_data_rate_set(&dev_ctx_imu, LSM9DS1_IMU_59Hz5);
  lsm9ds1_mag_data_rate_set(&dev_ctx_mag, LSM9DS1_MAG_UHP_10Hz);


  return 0;// return if OK
}

typedef union{
  int16_t i16bit[3];
  uint8_t u8bit[6];
} axis3bit16_t;


uint8_t  Imu_read_mag( float *mag_x, float *mag_y, float *mag_z)
{
	  lsm9ds1_dev_status_get(&dev_ctx_mag, &dev_ctx_imu, &reg);
	  axis3bit16_t data_raw_magnetic_field;
	  if (reg.status_mag.zyxda )
	    {
	      memset(data_raw_magnetic_field.u8bit, 0x00, 3 * sizeof(int16_t));
	      lsm9ds1_magnetic_raw_get(&dev_ctx_mag, data_raw_magnetic_field.u8bit);
	      *mag_x = lsm9ds1_from_fs16gauss_to_mG(data_raw_magnetic_field.i16bit[0]);
	      *mag_y = lsm9ds1_from_fs16gauss_to_mG(data_raw_magnetic_field.i16bit[1]);
	      *mag_z = lsm9ds1_from_fs16gauss_to_mG(data_raw_magnetic_field.i16bit[2]);
	      return 0;// return if OK
	    }
	  else{
		  return 1; // return in case of error
	  }
}
uint8_t Imu_read_gyro_accel( float *gyro_x, float *gyro_y, float *gyro_z,
		float *accel_x, float *accel_y, float *accel_z)
{
	  axis3bit16_t data_raw_acceleration;
	  axis3bit16_t data_raw_angular_rate;
	  lsm9ds1_dev_status_get(&dev_ctx_mag, &dev_ctx_imu, &reg);
	  if ( reg.status_imu.xlda && reg.status_imu.gda )
	     {
	       /* Read imu data */
	       memset(data_raw_acceleration.u8bit, 0x00, 3 * sizeof(int16_t));
	       memset(data_raw_angular_rate.u8bit, 0x00, 3 * sizeof(int16_t));

	       lsm9ds1_acceleration_raw_get(&dev_ctx_imu, data_raw_acceleration.u8bit);
	       lsm9ds1_angular_rate_raw_get(&dev_ctx_imu, data_raw_angular_rate.u8bit);

	       *accel_x = lsm9ds1_from_fs4g_to_mg(data_raw_acceleration.i16bit[0]);
	       *accel_y = lsm9ds1_from_fs4g_to_mg(data_raw_acceleration.i16bit[1]);
	       *accel_z = lsm9ds1_from_fs4g_to_mg(data_raw_acceleration.i16bit[2]);

	       *gyro_x = lsm9ds1_from_fs2000dps_to_mdps(data_raw_angular_rate.i16bit[0]);
	       *gyro_y = lsm9ds1_from_fs2000dps_to_mdps(data_raw_angular_rate.i16bit[1]);
	       *gyro_z = lsm9ds1_from_fs2000dps_to_mdps(data_raw_angular_rate.i16bit[2]);
	       return 0;// return if OK
	     }
	  else{
		  return 1; // return in case of error
	  }
}

