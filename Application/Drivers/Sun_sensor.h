/*
 * Sun_sensor.h
 *
 *  Created on: 7 мая 2021 г.
 *      Author: Ilia
 */

#ifndef DRIVERS_SUN_SENSOR_H_
#include "main.h"
#include "stdbool.h"
#define DATA_DMA_SIZE    8
#define STREAM_BUF_SIZE  16
#pragma push(pack)
#pragma pack(1)
struct sun_sensor_data {
	uint16_t angle1;
	uint16_t angle2;
	uint16_t luminosity;
};
#pragma pop(pack)
void Sun_sensor_init(UART_HandleTypeDef * huart);
bool Sun_sensor_get_data(struct sun_sensor_data *data, uint32_t timeout);
#endif /* DRIVERS_SUN_SENSOR_H_ */
