#include "flash.h"
#include "string.h"
uint32_t flash_erase(uint32_t address_start,uint16_t page_num){
	HAL_FLASH_Unlock();
	HAL_FLASH_OB_Unlock();
	uint16_t page = (address_start - FIRST_PAGE_ADDRESS)/FLASH_PAGE_SIZE;
	FLASH_EraseInitTypeDef EraseInitStruct;
	uint32_t PageError;
	volatile HAL_StatusTypeDef status;
	EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
	EraseInitStruct.Banks = address_start > FLASH_BANK1_END? FLASH_BANK_2: FLASH_BANK_1 ;
	EraseInitStruct.Page = page;
	EraseInitStruct.NbPages = page_num;
	status = HAL_FLASHEx_Erase(&EraseInitStruct, &PageError);
	HAL_FLASH_OB_Lock();
	HAL_FLASH_Lock();
	if(status != HAL_OK)
	{
		return PageError;
	}
	else
	{
		return 0;
	}
}

uint32_t flash_program_page(const uint8_t *data,const uint16_t page,const uint16_t pack_size)
{

	  /* Unlock the Flash to enable the flash control register access *************/
	  HAL_FLASH_Unlock();
	  /* Allow Access to option bytes sector */
	  HAL_FLASH_OB_Unlock();
	  /* Fill EraseInit structure*/
	  FLASH_EraseInitTypeDef EraseInitStruct;
	  EraseInitStruct.TypeErase = FLASH_TYPEERASE_PAGES;
	  EraseInitStruct.Banks = FLASH_BANK_1;
	  EraseInitStruct.Page = page;
	  EraseInitStruct.NbPages = 1;
	  uint32_t PageError;

	  volatile uint32_t write_cnt=0, index=0;
	  volatile HAL_StatusTypeDef status;

	  status = HAL_FLASHEx_Erase(&EraseInitStruct, &PageError);
	  if(status != HAL_OK)
	  {
		  return PageError;
	  }

	  while(index < pack_size/8)
	  {

		  status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, FIRST_PAGE_ADDRESS+FLASH_PAGE_SIZE*page+write_cnt, *(uint64_t*)&data[index*8]);
		  if(status != HAL_OK)
		  {
			  HAL_FLASH_OB_Lock();
			  HAL_FLASH_Lock();
			  return status;
		  }
		  write_cnt += 8;
		  index++;
	  }


	  HAL_FLASH_OB_Lock();
	  HAL_FLASH_Lock();
	  return 0;
}

uint32_t flash_save(void* data, uint32_t address, uint16_t data_length)
{
	uint64_t *data_to_FLASH  = data;
	/* Unlock the Flash to enable the flash control register access *************/
	HAL_FLASH_Unlock();
	/* Allow Access to option bytes sector */
	HAL_FLASH_OB_Unlock();
	//HAL_Delay(10);
	volatile uint32_t write_cnt=0, index=0;
	volatile HAL_StatusTypeDef status;
	while(index < data_length/8)
	{
		status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD, address+write_cnt,*(data_to_FLASH + index));
		if(status != HAL_OK)
		{
		    HAL_FLASH_OB_Lock();
		    HAL_FLASH_Lock();
			return status;
		}
		write_cnt += 8;
		index++;
	}

	HAL_FLASH_OB_Lock();
	HAL_FLASH_Lock();
	return 0;
}
/*data - pointer to data
n_words- 1 word= 4 bytes*/
void flash_read(uint8_t* data,uint32_t address, uint8_t n_words)
{
	volatile uint32_t read_data;
	volatile uint8_t read_cnt=0;
	for(uint8_t i = 0; i < n_words; i++)
	{
		if (read_data == 0xFFFFFFFF){
			break;
		}
			read_data = *(uint32_t*)(address + read_cnt);
			data[read_cnt] = (uint8_t)read_data;
			data[read_cnt + 1] = (uint8_t)(read_data >> 8);
			data[read_cnt + 2] = (uint8_t)(read_data >> 16);
			data[read_cnt + 3] = (uint8_t)(read_data >> 24);
			read_cnt += 4;

	}
}

