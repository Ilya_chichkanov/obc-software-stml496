/*
 * power.c
 *
 *  Created on: 29 апр. 2021 г.
 *      Author: Ilia
 */

#include "power.h"
#include "status.h"
#include "settings.h"

int power_sw_on(uint8_t channel){
	struct status * dev_status = status_get();
	switch (channel){
	   case CH_BB:
		   BB_ENABLE
		   dev_status->bb_en = ON;
		   break;
	   case CH_COLS:
		   COIL_ENABLE
		   dev_status->coil_en = ON;
		   break;
	   case CH_GPS:
		   GPS_ENABLE
		   dev_status->gps_en=ON;
		   break;
	   default:
		   break;
	}
	return 0;
}

int power_sw_off(uint8_t channel){
	struct status * dev_status = status_get();
	switch (channel){
	   case CH_BB:
		   BB_DISABLE
		   dev_status->bb_en = OFF;
		   break;
	   case CH_COLS:
		   COIL_DISABLE
		   dev_status->coil_en = OFF;
		   break;
	   case CH_GPS:
		   GPS_DISABLE
		   dev_status->gps_en=OFF;
		   break;
	   default:
		   break;
	}
	return 0;
}


