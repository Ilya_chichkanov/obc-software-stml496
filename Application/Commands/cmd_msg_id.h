/*
 * cmd_msg_id.h
 *
 *  Created on: 4 апр. 2021 г.
 *      Author: Ilia
 */
#define OBC                            1

#ifndef COMMANDS_CMD_MSG_ID_H_
#define COMMANDS_CMD_MSG_ID_H_
/*massage id for input commands*/
#define CMD_RESET_SOFTWARE             43760
#define CMD_GET_FIRMWARE_VERSION       65504
#define CMD_GET_ONBOARD_TIME           18000
#define CMD_FWUPDATE_PROGRAM           0xAB01
#define CMD_FWUPDATE_ERASE             0xAB00
#define CMD_GET_FWUPDATE_STATE         14751
#define CMD_GET_LOGBOOK_SIZE           17020
#define CMD_GET_LOGBOOK_RECORD         17022
#define CMD_LOGBOOK_ERASE              17024
#define CMD_SET_BOOTLOADER_FLAG        13000
/*massage id for output responces*/
#define RESP_VERSION_SW                0xFFE1
#define RESP_FWUPDATE_MEMSET_REPORT    0xAA03
#define RESP_FWUPDATE_STATE            46734
#define RESP_LOGBOOK_RECORD            17023
#define RESP_LOGBOOK_SIZE              17021
#define RESP_ONBOARD_TIME              18001
/*information pakages*/
#define INFO_FWUPDATE_SUCCESS          14752
#endif /* COMMANDS_CMD_MSG_ID_H_ */
