
#include "repo_cmd.h"
#include "cmd_msg_id.h"
#include "fwupdate.h"
#include "common_cmd.h"
#include "cmd_OBC.h"
static uint8_t cmd_table_size = 0;

const cmd_descr cmd_table[] = {
/*		common commads*/
		0xFFE3,						   prob_cmd,                            1,    0,
		CMD_GET_ONBOARD_TIME,          get_onboard_time,                    1,    0,
        CMD_GET_FIRMWARE_VERSION,      get_firmare_version,                 1,    0,
		CMD_RESET_SOFTWARE,            reset,                               1,    0,
/*		commands for remote firmware update*/
		CMD_FWUPDATE_PROGRAM,          fwupdate_program,                    0,    0,
		CMD_SET_BOOTLOADER_FLAG,       fwupdate_set_bootloader_flag,        1,    0,
		CMD_FWUPDATE_ERASE,            fwupdate_erase,                      1,    8,
		CMD_GET_FWUPDATE_STATE,        get_fwupdate_state,                  1,    0,
/*		commands for logbook*/
		CMD_GET_LOGBOOK_SIZE,          get_logbook_size,                    1,    0,
		CMD_GET_LOGBOOK_RECORD,        get_logbook_record,                  1,    2,
		CMD_LOGBOOK_ERASE,             logbook_erase,                       1,    0
};

uint16_t cmd_check(uint16_t msg_id, uint8_t n_bytes, uint8_t *cmd_table_id) {
    for (int ii = 0; ii < cmd_table_size; ii++) {
        if (cmd_table[ii].msg_id == msg_id) {
            *cmd_table_id = ii;
            if(cmd_table[ii].fix_cmd_size){
				if (n_bytes == cmd_table[ii].n_bytes) {
					return CMD_OK;
				} else {
					return CMD_WRONG_PARAM;
				}
            }
            else{
            	return CMD_OK;
            }
        }
    }
    return CMD_NOT_FOUND;
}

void cmd_repo_init() {
    cmd_table_size = sizeof(cmd_table) / sizeof(cmd_table[0]);
}

uint16_t cmd_run(uint8_t id, uint8_t sender, uint8_t *args) {
    return cmd_table[id].function(sender, args);
}
