#ifndef UNICAN_PROCESSOR_CAN_H
#define UNICAN_PROCESSOR_CAN_H

#include <stdint.h>
#include <stdbool.h>
#include "stm32l4xx_hal.h"
#include "FreeRTOS.h"
#include "message_buffer.h"
#include "semphr.h"

#define UNICAN_ACK                  280
#define UNICAN_NACK                 281
#define UNICAN_HEATBEAT             200

#define UNICAN_TIMEOUT_CMD          100
#define UNICAN_MAX_TIMEOUT_COUNT    3

#define UNICAN_IN_BUFFER_SIZE       256
#define UNICAN_OUT_BUFFER_SIZE      256
#define UNNICAN_OUT_QUEUE_SIZE      UNICAN_OUT_BUFFER_SIZE/sizeof(struct can_msg_tx)
#define UNICAN_RESPOND_BUFFER_SIZE  256

#define UNICAN_MAX_PAYLOAD_SIZE     256
#define UNICAN_SHORT_PAYLOAD_SIZE   6
#define UNICAN_LONG_PAYLOAD_SIZE    8

#define UNICAN_LONG_START_ID        0xFFFE
#define UNICAN_START_SIZE           6
#define UNICAN_DLC(len)            (len + 2u)

#define UNICAN_HANDLERID_GROUND     1
typedef enum {ACK, NACK} unican_resp_type;
typedef enum {OK,ERR,TIMEOUT,TRANSMIT_ERR,ANKNOWN_RESP} unican_cmd_resp_result;

#pragma push(pack)
#pragma pack(1)

struct can_msg_rx {
    CAN_RxHeaderTypeDef header;
    uint8_t data[8];
};

struct can_msg_tx {
    CAN_TxHeaderTypeDef header;
    uint8_t data[8];
};

struct unican_packet {
    uint16_t msg_id;
    uint16_t data_len;
    uint8_t sender;
    uint8_t data[UNICAN_IN_BUFFER_SIZE];
};

struct unican_canid {
    uint8_t receiver: 5;
    uint8_t sender: 5;
    uint8_t data: 1;
};

struct unican_short_msg {
    uint16_t msg_id;
    uint8_t payload[UNICAN_SHORT_PAYLOAD_SIZE];
};

struct unican_start_msg {
    uint16_t start_id;
    uint16_t msg_id;
    uint16_t len;
};

struct unican_data_msg {
    uint8_t payload[UNICAN_LONG_PAYLOAD_SIZE];
};

union unican_can_msg_data {
    struct unican_short_msg short_msg;
    struct unican_start_msg start_msg;
    struct unican_data_msg data_msg;
};

struct unican_message {
    uint16_t unican_msg_id; //MSG_ID of unican message
    uint8_t unican_address_to; // address of receiver in sattelite network
    uint8_t unican_length; //length of data
    uint8_t *data; //pointer to data field
};

struct unican_respond{
	uint8_t address;
	uint16_t msg_id;
	uint16_t value;
	unican_resp_type type;
};

struct unican_respond_result{
	uint16_t value;
	unican_cmd_resp_result type;
};
#pragma pop(pack)

struct unican_handler {
    uint8_t id;
    uint8_t in_message_buffer_mem[UNICAN_MAX_PAYLOAD_SIZE];
    StaticMessageBuffer_t in_message_buffer;
    struct unican_packet packet;
    struct unican_handler *next;
};

void unican_init(CAN_HandleTypeDef *hcan);

void unican_add_handler(uint8_t id, struct unican_handler *handler);

bool unican_receive(struct unican_handler *h, uint32_t timeout);

bool unican_send_msg(struct unican_message *unican_msg);

struct unican_respond_result unican_send_cmd( struct unican_message * unican_msg);

bool unican_ack(uint8_t dest_addr, uint16_t msg_id);

bool unican_nack(uint8_t dest_addr, uint16_t msg_id,uint16_t value);

bool unican_send_u16(uint8_t dest_addr, uint16_t value, uint16_t msg_id);

bool unican_hearbeat(uint8_t dest_addr);

#endif //UNICAN_PROCESSOR_CAN_H
