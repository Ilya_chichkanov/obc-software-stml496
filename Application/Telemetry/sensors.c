#include "stm32l4xx_hal.h"

#include "sensors.h"
#include "board.h"
#include "status.h"
#include "INA219.h"
#include "TMP100.h"
#include "errors.h"
#include "Imu_sens.h"
#include "PAC1934.h"

#include "ff_gen_drv.h"
#include "user_diskio.h"

extern I2C_HandleTypeDef hi2c1;
extern I2C_HandleTypeDef hi2c2;

extern UART_HandleTypeDef *muart;

void sensors_init() {
    struct status *status_dev = status_get();
    HAL_GPIO_WritePin(SPI1_CS_GPIO_Port, SPI1_CS_Pin, GPIO_PIN_RESET);
    uint16_t error_id;
    error_id  = TMP100_Init(TMP100_COILZ);;
    status_dev->sens_err[SENS_OBC_TERM100NA_COILZ]  = error_id==0? 0:ERR_TMP100_INIT;
    error_id = TMP100_Init(TMP100_COILXY);
    status_dev->sens_err[SENS_OBC_TERM100NA_COILXY] = error_id==0? 0:ERR_TMP100_INIT;
    error_id = TMP100_Init(TMP100_IMU);
    status_dev->sens_err[SENS_OBC_TERM100NA_IMU]    = error_id==0? 0:ERR_TMP100_INIT;
    error_id = TMP100_Init(TMP100_SW_3V3);
    status_dev->sens_err[SENS_OBC_TERM100NA_SW_3V3] = error_id==0? 0:ERR_TMP100_INIT;
    error_id = INA219_SetCalibration(&hi2c1,INA219_BB_VCC);
    status_dev->sens_err[SENS_OBC_INA219_BB_VCC]    = error_id==0? 0:ERR_INA219_BB_VCC;
    error_id = INA219_SetCalibration(&hi2c1,INA219_COIL_VCC);
    status_dev->sens_err[SENS_OBC_INA219_COIL_VCC]  =  error_id==0? 0:ERR_INA219_COIL_VCC;
    error_id= INA219_SetCalibration(&i2c_INA219,INA219_OBC_3V3);
    status_dev->sens_err[SENS_OBC_INA219_OBC_3V3]   =  error_id==0? 0:ERR_INA219_OBC_3V3;
    error_id                                        = Imu_init();
    status_dev->sens_err[SENS_OBC_LSM9DS1_MAG]      =  error_id==0? 0:ERR_IMU_MAG;
    status_dev->sens_err[SENS_OBC_LSM9DS1_GYRO]     =  error_id==0? 0:ERR_IMU_GYRO;
    HAL_GPIO_WritePin(PAC1934_SLOW_GPIO_Port, PAC1934_SLOW_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(PAC1934_PWRDN_GPIO_Port, PAC1934_PWRDN_Pin, GPIO_PIN_SET);
    error_id                                 = PAC1934_Init(&i2c_PAC1934);
    status_dev->sens_err[SENS_OBC_PAC1934_COL_X]     = error_id==0? 0:ERR_PAC1934_INIT;;
    status_dev->sens_err[SENS_OBC_PAC1934_COL_Y]     = error_id==0? 0:ERR_PAC1934_INIT;
    status_dev->sens_err[SENS_OBC_PAC1934_COL_Z]     = error_id==0? 0:ERR_PAC1934_INIT;
    error_id =  !((sdcard_init() == RES_OK) && (sdcard_test_stat() == 0) && (sdcard_test_write("test.txt") == 0));
    status_dev->sens_err[SENS_OBC_SD_CARD] =   error_id==0? 0:ERR_SD_CARD_INIT;

    for (uint8_t i = 0; i < SENS_OBC_COUNT; i++) {
        if (status_dev->sens_err[i] != 0) {
            char str_send[30];
            sprintf(str_send, "ERROR WHILE INIT SENSOR %d\r\n", i);
            HAL_UART_Transmit(muart, (uint8_t *) str_send, strlen((char *) str_send), 100);
        }
    }

    i2c_Scan(&hi2c1);
    i2c_Scan(&hi2c2);

}
